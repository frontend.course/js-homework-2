let name = null

while (name === null || name === "") {
    name = prompt('Введіть ваше ім\'я');
}

let age = null
while (age === null || age === "" || isNaN(age)) {
    age = prompt('Скільки вам років?')
}
function showNotAllowed() {
    alert('You are not allowed to visit this website.')
}

function showWelcome(name) {
    alert(`Welcome ${name}`)
}

if (age < 18) {
    showNotAllowed()
} else if (age <= 22) {
    const agreement = confirm('Are you sure you want to continue?')
    if (agreement) {
        showWelcome(name)
    } else {
        showNotAllowed()
    }
} else {
    showWelcome(name)
}